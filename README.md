## A simple poker game

A game of poker, built in React, where one human player competes with a computer opponent.

## Setting up

To play the game locally, you just need index.html and main.js from the dist folder, along with the images in src/images.

If uploading to a web server, there are additional files that make the game playable as a progressive web app, which, after it's been loaded once, makes the game available to play even if the user is offline.

The additional files required are:

- icon-192.png
- icon-512.png
- manifest.json
- sw.js