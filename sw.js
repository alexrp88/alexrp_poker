var cacheName = 'pokache';
var filesToCache = [
  '/game/poker/index.html',
  '/game/poker/main.js',
  '/game/src/images/10C.svg', 
  '/game/src/images/10D.svg', 
  '/game/src/images/10H.svg', 
  '/game/src/images/10S.svg', 
  '/game/src/images/2C.svg', 
  '/game/src/images/2D.svg', 
  '/game/src/images/2H.svg', 
  '/game/src/images/2S.svg', 
  '/game/src/images/3C.svg', 
  '/game/src/images/3D.svg', 
  '/game/src/images/3H.svg', 
  '/game/src/images/3S.svg', 
  '/game/src/images/4C.svg', 
  '/game/src/images/4D.svg', 
  '/game/src/images/4H.svg', 
  '/game/src/images/4S.svg', 
  '/game/src/images/5C.svg', 
  '/game/src/images/5D.svg', 
  '/game/src/images/5H.svg', 
  '/game/src/images/5S.svg', 
  '/game/src/images/6C.svg', 
  '/game/src/images/6D.svg', 
  '/game/src/images/6H.svg', 
  '/game/src/images/6S.svg', 
  '/game/src/images/7C.svg', 
  '/game/src/images/7D.svg', 
  '/game/src/images/7H.svg', 
  '/game/src/images/7S.svg', 
  '/game/src/images/8C.svg', 
  '/game/src/images/8D.svg', 
  '/game/src/images/8H.svg', 
  '/game/src/images/8S.svg', 
  '/game/src/images/9C.svg', 
  '/game/src/images/9D.svg', 
  '/game/src/images/9H.svg', 
  '/game/src/images/9S.svg', 
  '/game/src/images/AC.svg', 
  '/game/src/images/AD.svg', 
  '/game/src/images/AH.svg', 
  '/game/src/images/AS.svg', 
  '/game/src/images/JC.svg', 
  '/game/src/images/JD.svg', 
  '/game/src/images/JH.svg', 
  '/game/src/images/JS.svg', 
  '/game/src/images/KC.svg', 
  '/game/src/images/KD.svg', 
  '/game/src/images/KH.svg', 
  '/game/src/images/KS.svg', 
  '/game/src/images/QC.svg', 
  '/game/src/images/QD.svg', 
  '/game/src/images/QH.svg', 
  '/game/src/images/QS.svg', 
  '/game/src/images/rev.svg'
];

// Cache content
self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('fetch', function(e) {
  
  //Go to network first for root object if online
  if (e.request.url.endsWith('.html') || e.request.url.endsWith('/') || e.request.url.endsWith('/poker'))
  {
	if (navigator.onLine)
	{
		e.respondWith(
		fetch(e.request).catch(function() {
				return caches.match(e.request);
			})
		);
	}
	else
	{
		//get from cache ... or last ditch attempt to go to network
		e.respondWith(
		caches.match(e.request).then(function(response) {
		return response || fetch(e.request);
    })
	);
	}
  }
  //else cache first then network
  else
  {
	e.respondWith(
    caches.match(e.request).then(function(response) {
    return response || fetch(e.request);
    })
  );
  }
});
