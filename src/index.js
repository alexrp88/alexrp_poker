import React from "react"
import ReactDOM from "react-dom"
import Setup from "./components/Setup"

ReactDOM.render(<Setup />,document.getElementById("root"))