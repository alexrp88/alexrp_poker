const cards = [
	{
	"id": 1,
	"suit": "clubs",
	"value": "2",
	"score": 2,
	"drawn": false,
	"img": "../src/images/2C.svg"
	},
	{
	"id": 2,
	"suit": "clubs",		
	"value": "3",
	"score": 3,
	"drawn": false,
	"img": "../src/images/3C.svg"
	},
	{
	"id": 3,
	"suit": "clubs",		
	"value": "4",
	"score": 4,
	"drawn": false,
	"img": "../src/images/4C.svg"
	},
	{
	"id": 4,
	"suit": "clubs",		
	"value": "5",
	"score": 5,
	"drawn": false,
	"img": "../src/images/5C.svg"
	},
	{
	"id": 5,
	"suit": "clubs",		
	"value": "6",
	"score": 6,
	"drawn": false,
	"img": "../src/images/6C.svg"
	},
	{
	"id": 6,
	"suit": "clubs",		
	"value": "7",
	"score": 7,
	"drawn": false,
	"img": "../src/images/7C.svg"
	},
	{
	"id": 7,
	"suit": "clubs",		
	"value": "8",
	"score": 8,
	"drawn": false,
	"img": "../src/images/8C.svg"
	},
	{ 
	"id": 8,
	"suit": "clubs",
	"value": "9",
	"score": 9,
	"drawn": false,
	"img": "../src/images/9C.svg"
	},
	{
	"id": 9,
	"suit": "clubs",		
	"value": "10",
	"score": 10,
	"drawn": false,
	"img": "../src/images/10C.svg"
	},
	{ 
	"id": 10,
	"suit": "clubs",
	"value": "Jack",
	"score": 11,
	"drawn": false,
	"img": "../src/images/JC.svg"
	},
	{
	"id": 11,
	"suit": "clubs",		
	"value": "Queen",
	"score": 12,
	"drawn": false,
	"img": "../src/images/QC.svg"
	},
	{
	"id": 12,
	"suit": "clubs",		
	"value": "King",
	"score": 13,
	"drawn": false,
	"img": "../src/images/KC.svg"
	},
	{ 
	"id": 13,
	"suit": "clubs",
	"value": "Ace",
	"score": 14,
	"drawn": false,
	"img": "../src/images/AC.svg"
	},	
	{ 
	"id": 14,
	"suit": "hearts",	
	"value": "2",
	"score": 2,
	"drawn": false,
	"img": "../src/images/2H.svg"
	},
	{ 	
	"id": 15,
	"suit": "hearts",	
	"value": "3",
	"score": 3,
	"drawn": false,
	"img": "../src/images/3H.svg"
	},
	{ 	
	"id": 16,
	"suit": "hearts",	
	"value": "4",
	"score": 4,
	"drawn": false,
	"img": "../src/images/4H.svg"
	},
	{ 	
	"id": 17,
	"suit": "hearts",	
	"value": "5",
	"score": 5,
	"drawn": false,
	"img": "../src/images/5H.svg"
	},
	{ 	
	"id": 18,
	"suit": "hearts",	
	"value": "6",
	"score": 6,
	"drawn": false,
	"img": "../src/images/6H.svg"
	},
	{ 	
	"id": 19,
	"suit": "hearts",	
	"value": "7",
	"score": 7,
	"drawn": false,
	"img": "../src/images/7H.svg"
	},
	{ 	
	"id": 20,
	"suit": "hearts",	
	"value": "8",
	"score": 8,
	"drawn": false,
	"img": "../src/images/8H.svg"
	},
	{ 	
	"id": 21,
	"suit": "hearts",	
	"value": "9",
	"score": 9,
	"drawn": false,
	"img": "../src/images/9H.svg"
	},
	{ 	
	"id": 22,
	"suit": "hearts",	
	"value": "10",
	"score": 10,
	"drawn": false,
	"img": "../src/images/10H.svg"
	},
	{ 	
	"id": 23,
	"suit": "hearts",	
	"value": "Jack",
	"score": 11,
	"drawn": false,
	"img": "../src/images/JH.svg"
	},
	{ 	
	"id": 24,
	"suit": "hearts",	
	"value": "Queen",
	"score": 12,
	"drawn": false,
	"img": "../src/images/QH.svg"
	},
	{ 	
	"id": 25,
	"suit": "hearts",	
	"value": "King",
	"score": 13,
	"drawn": false,
	"img": "../src/images/KH.svg"
	},
	{ 	
	"id": 26,
	"suit": "hearts",	
	"value": "Ace",
	"score": 14,
	"drawn": false,
	"img": "../src/images/AH.svg"
	},
	{
	"id": 27,
	"suit": "diamonds",
	"value": "2",
	"score": 2,
	"drawn": false,
	"img": "../src/images/2D.svg"
	},
	{ 	
	"id": 28,
	"suit": "diamonds",	
	"value": "3",
	"score": 3,
	"drawn": false,
	"img": "../src/images/3D.svg"
	},
	{ 	
	"id": 29,
	"suit": "diamonds",
	"value": "4",
	"score": 4,
	"drawn": false,
	"img": "../src/images/4D.svg"
	},
	{ 	
	"id": 30,
	"suit": "diamonds",
	"value": "5",
	"score": 5,
	"drawn": false,
	"img": "../src/images/5D.svg"
	},
	{ 	
	"id": 31,
	"suit": "diamonds",
	"value": "6",
	"score": 6,
	"drawn": false,
	"img": "../src/images/6D.svg"
	},
	{ 	
	"id": 32,
	"suit": "diamonds",
	"value": "7",
	"score": 7,
	"drawn": false,
	"img": "../src/images/7D.svg"
	},
	{ 	
	"id": 33,
	"suit": "diamonds",
	"value": "8",
	"score": 8,
	"drawn": false,
	"img": "../src/images/8D.svg"
	},
	{ 	
	"id": 34,
	"suit": "diamonds",
	"value": "9",
	"score": 9,
	"drawn": false,
	"img": "../src/images/9D.svg"
	},
	{ 	
	"id": 35,
	"suit": "diamonds",
	"value": "10",
	"score": 10,
	"drawn": false,
	"img": "../src/images/10D.svg"
	},
	{ 	
	"id": 36,
	"suit": "diamonds",
	"value": "Jack",
	"score": 11,
	"drawn": false,
	"img": "../src/images/JD.svg"
	},
	{ 	
	"id": 37,
	"suit": "diamonds",
	"value": "Queen",
	"score": 12,
	"drawn": false,
	"img": "../src/images/QD.svg"
	},
	{ 	
	"id": 38,
	"suit": "diamonds",
	"value": "King",
	"score": 13,
	"drawn": false,
	"img": "../src/images/KD.svg"
	},
	{ 	
	"id": 39,
	"suit": "diamonds",
	"value": "Ace",
	"score": 14,
	"drawn": false,
	"img": "../src/images/AD.svg"
	},
	{
	"id": 40,
	"suit": "spades",
	"value": "2",
	"score": 2,
	"drawn": false,
	"img": "../src/images/2S.svg"
	},
	{ 	
	"id": 41,
	"suit": "spades",
	"value": "3",
	"score": 3,
	"drawn": false,
	"img": "../src/images/3S.svg"
	},
	{ 	
	"id": 42,
	"suit": "spades",
	"value": "4",
	"score": 4,
	"drawn": false,
	"img": "../src/images/4S.svg"
	},
	{ 	
	"id": 43,
	"suit": "spades",
	"value": "5",
	"score": 5,
	"drawn": false,
	"img": "../src/images/5S.svg"
	},
	{ 	
	"id": 44,
	"suit": "spades",
	"value": "6",
	"score": 6,
	"drawn": false,
	"img": "../src/images/6S.svg"
	},
	{ 	
	"id": 45,
	"suit": "spades",
	"value": "7",
	"score": 7,
	"drawn": false,
	"img": "../src/images/7S.svg"
	},
	{ 	
	"id": 46,
	"suit": "spades",
	"value": "8",
	"score": 8,
	"drawn": false,
	"img": "../src/images/8S.svg"
	},
	{ 	
	"id": 47,
	"suit": "spades",
	"value": "9",
	"score": 9,
	"drawn": false,
	"img": "../src/images/9S.svg"
	},
	{ 	
	"id": 48,
	"suit": "spades",
	"value": "10",
	"score": 10,
	"drawn": false,
	"img": "../src/images/10S.svg"
	},
	{ 	
	"id": 49,
	"suit": "spades",
	"value": "Jack",
	"score": 11,
	"drawn": false,
	"img": "../src/images/JS.svg"
	},
	{ 	
	"id": 50,
	"suit": "spades",
	"value": "Queen",
	"score": 12,
	"drawn": false,
	"img": "../src/images/QS.svg"
	},
	{ 	
	"id": 51,
	"suit": "spades",
	"value": "King",
	"score": 13,
	"drawn": false,
	"img": "../src/images/KS.svg"
	},
	{ 	
	"id": 52,
	"suit": "spades",
	"value": "Ace",
	"score": 14,
	"drawn": false,
	"img": "../src/images/AS.svg"
	}
]
export default cards