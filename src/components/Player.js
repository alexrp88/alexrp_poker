import React, {Component} from "react"

class Player extends Component {
	constructor() {
		super()

	}

	render() {
		let chipsArray = []
		for (var i=1; i <= this.props.chips; i++)
		{
			chipsArray.push(i)
		}
		return(
		<div>
		<h3>{this.props.name} - chips: {this.props.chips}</h3>
		<button style={{display: this.props.hand.length < 5 && this.props.turn == 0 ? "block" : "none"}} onClick={event => this.props.deal(this.props.playerNum)}>Deal to player</button>
		<div className="container">
			{
				this.props.hand.map(card => {
					return <div className="cardHolder" key={card.id}><img className="card" title={this.props.character == 0  || this.props.turn == 6 ? card.value + " of " + card.suit : ""} src={this.props.character == 0 || this.props.turn == 6 || this.props.devMode ? card.img : "../src/images/rev.svg"} /><br />
					<div className="discardContainer" style={{display: this.props.character == 0 && this.props.turn == 2 && this.props.discarded == false && this.props.drawn == false && this.props.buffer.length < 3 ? "inline-block" : "none"}} >
					<button className="discardButton" onClick={event => this.props.discard(this.props.playerNum, card.id)}>Discard</button>
					</div>
				</div>
				})
			}
		</div>
		<div style={{display: !this.props.gameOver && !this.props.readyNext && !this.props.won && this.props.myturn && (this.props.turn == 1 || this.props.turn == 4) ? "block" : "none"}}>

		<div style={{display: this.props.character == 0 ? "block" : "none"}}>
		<p>Check, choose how many chips you want to bet or fold:</p>	
		<div className="container">
		<button style={{display: this.props.highestBet - this.props.currentBet < 1 || this.props.chips == 0 ? "block" : "none" }} onClick={event => this.props.check(this.props.playerNum)}>Check</button>
		{/*Check have chips and make betting amounts available only if that amount is enough to cover difference between highest bet and player's current stake */}
		
		<div style={{display: this.props.chips > 0 && this.props.chips > this.props.highestBet - this.props.currentBet ? "inline" : "none" }}>
		
		{chipsArray.map(chipNum => { return  <button key={chipNum} style={{display: (this.props.chips >= chipNum && this.props.highestBet - this.props.currentBet <= chipNum) && ((this.props.raised === true && this.props.currentBet + chipNum <= this.props.highestBet) || (this.props.raised === false)) ? "inline" : "none"}} onClick={event => this.props.bet(this.props.playerNum,chipNum)}>{chipNum}</button> } )}

		</div>
		<button style={{display: (this.props.chips > 0)  && ((this.props.raised === true && this.props.currentBet + this.props.chips <= this.props.highestBet) || (this.props.raised === false && this.props.currentBet + this.props.chips <= this.props.highestBet)) ? "inline" : "none"}} onClick={event => this.props.bet(this.props.playerNum,this.props.chips)}>All in</button>
		<button onClick={event => this.props.fold(this.props.playerNum)}>Fold</button>
		</div>

		</div>
		
		<div style={{display: this.props.character > 0 ? "block" : "none"}}>
		{/*Arguments are: player number, able to check, have enough chips to bet */}
		<button onClick={event => this.props.compMove(this.props.playerNum,this.props.highestBet - this.props.currentBet < 1 || this.props.chips == 0)}>Find out what {this.props.name} does next</button>
	
		</div>
		</div>
		
		<div style={{display: this.props.character > 0 ? "block" : "none"}}>
			<button style={{display: this.props.discarded == false && this.props.turn == 2 ? "block" : "none"}} onClick={event => this.props.compDiscard(this.props.playerNum)}>Find out how many cards {this.props.name} discards</button>
		</div>
		<div style={{display: this.props.character == 0 ? "block" : "none"}}><p style={{display: this.props.discarded == false && this.props.turn == 2 ? "block" : "none"}}>Discard up to three cards you want to replace and hit the <em>Finished Discarding</em> button when you're done.</p>
		<button style={{display: this.props.discarded == false && this.props.turn == 2 ? "block" : "none"}} onClick={event => this.props.discardDone(this.props.playerNum)}>Finished discarding</button>
		</div>
		<button style={{display: this.props.hand.length < 5 && this.props.turn == 3 ? "block" : "none"}} onClick={event => this.props.deal(this.props.playerNum)}>Draw</button>
		
		<hr />
		</div>
		
		)
	}
}

export default Player