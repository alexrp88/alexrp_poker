import React, {Component} from "react"
import Player from "./Player"
import deck from "./deck"

class Setup extends Component {
	constructor() {
		super()
		this.state = {
			players: [],
			round: 1,
			started: false,
			cards: {},
			highestBet: 0,
			pot: 0,
			turn: 0,
			readyNext: false,
			computerPlayer: 1,
			gameOver: false,
			enoughPlayersWithChips: true,
			statusUpdate: "",
			odd: true,
			devMode: false
		}
	this.handleChange = this.handleChange.bind(this)
	this.handleClickSetup = this.handleClickSetup.bind(this)
	this.checkState = this.checkState.bind(this)
	this.abandon = this.abandon.bind(this)
	this.discard = this.discard.bind(this)
	this.discardDone = this.discardDone.bind(this)
	this.selectCard = this.selectCard.bind(this)	
	this.updateDeck = this.updateDeck.bind(this)
	this.updateHand = this.updateHand.bind(this)
	this.deal = this.deal.bind(this)
	this.next = this.next.bind(this)	
	this.bet = this.bet.bind(this)
	this.check = this.check.bind(this)
	this.fold = this.fold.bind(this)
	this.checkDone = this.checkDone.bind(this)
	this.checkHands = this.checkHands.bind(this)
	this.updateScore = this.updateScore.bind(this)
	this.compMove = this.compMove.bind(this)
	this.compDiscard = this.compDiscard.bind(this)
	this.newRound = this.newRound.bind(this)
	}
	componentDidMount() {
			this.setState(
			{
				cards: deck
			}
			)
	}

	handleClickSetup() {
		if (!document.getElementById("p_a").value || !document.getElementById("p_b").value)
		{
			alert("At least two players are needed!")
		}
		else
		{
			//Random number between 1 and 3 determines computer approach to risk - 1 more conservative, 3 more aggressive; 0 is human controlled player
			//const randomCharacter = Math.floor(Math.random() * 3) + 1
			const playerSet = []
			if (document.getElementById("p_a").value)
			{
				// Assign computer characteristics if P1 is computer; otherwise set to 0 (human) - needs refactoring - duplication
				const randomCharacter = this.state.computerPlayer == 0 ? Math.floor(Math.random() * 3) + 1 : 0
				const starting = randomCharacter == 0 ? true : false
				playerSet.push({
					id: 0,
					name: document.getElementById("p_a").value,
					hand: [],
					highestCard: 0,
					pairOneValue: 0,
					pairTwoValue: 0,
					threeKindValue: 0,
					fourKindValue: 0,
					tieBreakValues: [],
					buffer: [],
					chips: 10,
					currentBet: 0,
					maxWinnings:0,
					discarded: false,
					drawn: false,
					myturn: false,
					startingPlayer: starting,
					check: false,
					raised: false,
					won: false,
					handScore: 0,
					character: randomCharacter
					})
			}
			if (document.getElementById("p_b").value)
			{
				// Assign computer characteristics if P2 is computer; otherwise set to 0 (human) - needs refactoring
				const randomCharacter = this.state.computerPlayer == 1 ? Math.floor(Math.random() * 3) + 1 : 0
				const starting = randomCharacter == 0 ? true : false
				playerSet.push({
					id: 1,
					name: document.getElementById("p_b").value,
					hand: [],
					highestCard: 0,
					pairOneValue: 0,
					pairTwoValue: 0,
					threeKindValue: 0,
					fourKindValue: 0,
					tieBreakValues: [],
					buffer: [],
					chips: 10,
					currentBet: 0,
					maxWinnings:0,
					discarded: false,
					drawn: false,
					myturn: false,
					startingPlayer: starting,
					check: false,
					raised: false,
					won: false,
					handScore: 0,
					character: randomCharacter
				})
			}
			this.setState(
				{
				players : playerSet,
				started: true,
				turn: 0
				}
			)
		}
	}
	
	handleChange(event) {
		const {name, value, type, checked} = event.target
		if ([name] == "computerPlayer")
		{
			const numVal = parseInt(value)
			this.setState({[name]: numVal})
		}
		else
		{
			this.setState({[name]: value})
		}
	}
	
	abandon() {
		this.setState(prevState => {
		const newCards = prevState.cards
		for (var i=0;i<52;i++)
		{
			newCards[i].drawn = false
		}
		return {
			players: [],
			cards: newCards,
			round: 1,
			started: false,
			highestBet: 0,
			pot: 0,
			turn: 0,
			readyNext: false,
			computerPlayer: 1,
			gameOver: false,
			odd: true,
			enoughPlayersWithChips: true,
			statusUpdate: ""
		}
		})
	}

	newRound() {
	this.setState(prevState => {
		const roundNum = prevState.round + 1
		const newCards = prevState.cards
		for (var i=0;i<52;i++)
		{
			newCards[i].drawn = false
		}
		let newPlayers = prevState.players
		//Get player 1
		const playerOneObj = newPlayers.shift()
		playerOneObj.startingPlayer = !playerOneObj.startingPlayer
		newPlayers[0].startingPlayer = !newPlayers[0].startingPlayer
		//Move player 1 to second position in array
		newPlayers.push(playerOneObj)
		for (var i=0; i< newPlayers.length; i++)
		{
			//Update IDs so that id remains equal to position in array
			newPlayers[i].id = i
			newPlayers[i].hand = []
			newPlayers[i].highestCard = 0
			newPlayers[i].pairOneValue = 0
			newPlayers[i].pairTwoValue = 0
			newPlayers[i].threeKindValue = 0
			newPlayers[i].fourKindValue = 0
			newPlayers[i].tieBreakValues = []
			newPlayers[i].buffer = []
			newPlayers[i].currentBet = 0
			newPlayers[i].maxWinnings = 0
			newPlayers[i].discarded = false
			newPlayers[i].drawn = false
			newPlayers[i].myturn = false
			newPlayers[i].check = false
			newPlayers[i].raised = false
			newPlayers[i].won = false
			newPlayers[i].handScore = 0
		}
		return {
			players: newPlayers,
			cards: newCards,
			round: roundNum,
			gameOver: false,
			started: true,
			highestBet: 0,
			odd: true,
			statusUpdate: "",
			readyNext: false,
			turn: 0,
			pot: 0
		}
		})
	}
	
	checkState() {
		console.log(this.state)
	}
	
	next() {
		let advanceStep = 1
		const checkDeal = this.state.players.filter(player => {return player.hand.length <5})
		if (checkDeal.length == 0 && this.state.turn == 2)
		{
			//Advance twice if no cards are discarded at draw stage - avoids clicking Next twice
			advanceStep = 2
		}
		this.setState(prevState => {
			const newTurn = prevState.turn + advanceStep
			return {
				turn: newTurn,
				readyNext: false
			}
		}, () => {
			if (this.state.turn == 1)
			{
				this.setState(prevState => {
					const newPlayers = prevState.players
					if (newPlayers[0].startingPlayer === true)
					{
						newPlayers[0].myturn = true						
						newPlayers[1].myturn = false						
					}
					else
					{
						newPlayers[1].myturn = true	
						newPlayers[0].myturn = false												
					}
					return {
						players: newPlayers
					}
				})
			}
			if (this.state.turn == 3)
			{
				const checkDeal = this.state.players.filter(player => {return player.hand.length <5})
				this.setState(prevState => {
				let finishedDealing = true
				if (checkDeal.length > 0)
				{
					finishedDealing = false
				}
				return {
					readyNext: finishedDealing
				}
				})
			}
			
		}
		)
	}
	
	selectCard() {
		//filter deck to drawn cards
		const availableCards = this.state.cards.filter(function(aCard) { return aCard.drawn === false } )
		//get length of new array
		const numberOfCards = availableCards.length;
		//stop if no cards available
		if (numberOfCards == 0)
		{
			return false
		}
		//select random number between 0 and length -1
		const randomNumber = Math.floor(Math.random() * numberOfCards)
		//select card
		const cardPicked = availableCards[randomNumber]
		return cardPicked
	}
	
	updateDeck(cardPicked) {
		this.setState(prevState => {
		//get existing array of cards and copy into new array that will become new state for cards
		const newCards = prevState.cards
		//identify index of drawn card
		const drawnCard = prevState.cards.indexOf(cardPicked)
		//update the drawn property in the new array
		newCards[drawnCard].drawn = true
		//replace cards state with new arrary
		return {
			cards: newCards
		}
		})
	}

	bet(playerNum,amount) {
		this.setState(prevState => {
			let newOdd = prevState.odd
			const newPlayers = prevState.players
			let checkHighestBet = prevState.highestBet
			let newStatus = ""
			newPlayers[playerNum].chips = newPlayers[playerNum].chips - amount
			newPlayers[playerNum].currentBet = newPlayers[playerNum].currentBet + amount
			const newPot = prevState.pot + amount
			if (this.state.turn == 0)
			{
				newPlayers[playerNum].maxWinnings = 2
				newStatus = "The game begins"
			}
			else if (newPlayers[playerNum].currentBet >= checkHighestBet)
			{
				newPlayers[playerNum].maxWinnings = newPot
			}
			else
			{
				newPlayers[playerNum].maxWinnings = newPlayers[playerNum].currentBet * newPlayers.length
			}
			if (newPlayers[playerNum].currentBet > checkHighestBet)
			{
				if (checkHighestBet > 0)
				{
					newPlayers[playerNum].raised = true
				}
				checkHighestBet = newPlayers[playerNum].currentBet
			}
			if (newPlayers[playerNum].myturn)
			{
				newPlayers[playerNum].myturn = false
				if (newPlayers[playerNum + 1])
				{
					newPlayers[playerNum + 1].myturn = true
				}
				else
				{
					newPlayers[0].myturn = true
				}
			}
			if (this.state.turn > 0)
			{
				newOdd = !newOdd
				newStatus = newPlayers[playerNum].name + " bets " + amount				
			}
			return {
				odd: newOdd,
				statusUpdate: newStatus,
				players: newPlayers,
				pot: newPot,
				highestBet: checkHighestBet
			}
			}, () => {this.checkDone()} )
	}

	check(playerNum) {
		this.setState(prevState => {
			let newPlayers = prevState.players
			let newOdd = prevState.odd
			newOdd = !newOdd
			let checkHighestBet = prevState.highestBet
			newPlayers[playerNum].myturn = false
			newPlayers[playerNum].check = true
			if (newPlayers[playerNum].currentBet >= checkHighestBet)
			{
				newPlayers[playerNum].maxWinnings = prevState.pot
			}
			if (newPlayers[playerNum + 1])
			{
				newPlayers[playerNum + 1].myturn = true
			}
			else
			{
				newPlayers[0].myturn = true
			}
			const newStatus = newPlayers[playerNum].name + " checks"
			return {
				odd: newOdd,
				statusUpdate: newStatus,
				players: newPlayers
			}
		}, () => {this.checkDone()})
	}	
	
	fold(playerNum) {
		this.setState(prevState => {
			let newPlayers = prevState.players
			let newPot = prevState.pot
			const winner = newPlayers.find(player => player.id != playerNum)
			const winnderIndex = newPlayers.indexOf(winner)
			const newStatus = prevState.players[playerNum].name + " folds and " + prevState.players[winnderIndex].name + " wins!"
			newPlayers[winnderIndex].chips += newPlayers[winnderIndex].maxWinnings
			newPlayers[playerNum].chips += newPot - newPlayers[winnderIndex].maxWinnings
			newPlayers[winnderIndex].won = true
			const playersWithChips = newPlayers.filter(player => {return player.chips >0}).length > 1 ? true : false
			return {
				enoughPlayersWithChips: playersWithChips,
				statusUpdate: newStatus,
				players: newPlayers,
				pot: 0,
				gameOver: true
			}
		}, () => {this.checkDone()}
	)
	}	
	checkDone() {
		// check turn
		if (this.state.turn == 4)
		{
			// every player has checked 
			//const stillIn = this.state.players.filter(aPlayer => {return aPlayer.chips < 1 || aPlayer.check === true})
			const stillIn = this.state.players.filter(aPlayer => {return aPlayer.check === true})
			if (stillIn.length == this.state.players.length)
			{
				this.setState({
					readyNext: true
				})
			}	
		}
		else
		{
			const anyPlayerNotReady = this.state.players.filter(aPlayer => {return aPlayer.check === false})
				if (anyPlayerNotReady.length == 0)
				{
					this.setState(prevState => {
						const newTurn = prevState.turn + 1
						let newPlayers = prevState.players
						newPlayers[0].check = false
						newPlayers[1].check = false
						newPlayers[0].raised = false
						newPlayers[1].raised = false
						//newPlayers.map(aPlayer => {return aPlayer.check = false })
						//newPlayers.map(aPlayer => {return aPlayer.raised = false })
						return {
							turn: newTurn,
							players: newPlayers
						}
					})
				}
		}
	}
	
	checkHands() {
		const numPlayers = this.state.players.length
		//note player id is same as player index
		let leaderId
		let tiedWithId
		let highScore = 0;
		let tie = false
		for (var i = 0; i < numPlayers; i++)
		{
			if (this.state.players[i].handScore > highScore)
			{
				highScore = this.state.players[i].handScore
				leaderId = this.state.players[i].id
				// If score was tied before, reset tie to false
				if (tie == true)
				{
					tie = false
				}
			}
			else if (this.state.players[i].handScore == highScore)
			{
				// Set tie to true
				tie = true
				let tieBreakLength = this.state.players[i].tieBreakValues.length
				// Check tie break array, starting at end, where highest values are
				for (var j = tieBreakLength - 1; j < tieBreakLength && j >= 0; j--)
				{
					// If high card in this place of tie break array for this player is greater than that of the current leader, replace leader
					if (this.state.players[i].tieBreakValues[j] > this.state.players[leaderId].tieBreakValues[j])
					{
						leaderId = this.state.players[i].id
						// Tie is false if player's card beats current leader's
						tie = false
						// break out of loop as soon as one player's card beats the other's
						break;
					}
					else if (this.state.players[i].tieBreakValues[j] < this.state.players[leaderId].tieBreakValues[j])
					{
						// Tie is false if player's card is less than current leaders
						tie = false
					}
				}
				// If tie is still true after running through for loop, give player in tiedWithId
				if (tie == true)
				{
					tiedWithId = this.state.players[i].id
				}
			}
		}
		this.setState(prevState => {
			let newPlayers = prevState.players
			let newPot = prevState.pot
			newPlayers[leaderId].won = true
			let newStatus = newPlayers[leaderId].name + " won!"
			let newTurn = prevState.turn + 1
			if (tie == true)
			{
				newPlayers[tiedWithId].won = true
				newStatus = newPlayers[leaderId].name + " and " + newPlayers[tiedWithId].won + " tied!"
				if (newPlayers[tiedWithId].maxWinnings < newPot && newPlayers[leaderId].maxWinnings < newPot)
				{
					let tiedWinnings = Math.round(newPlayers[tiedWithId].maxWinnings/2)
					let leaderWinnings = Math.round(newPlayers[leaderId].maxWinnings/2)
					let remainingPot = Math.round((newPot - tiedWinnings - leaderWinnings)/2)
					newPlayers[tiedWithId].chips = newPlayers[tiedWithId].chips + tiedWinnings + remainingPot
					newPlayers[leaderId].chips = newPlayers[leaderId].chips + leaderWinnings + remainingPot
					newPot = 0
				}
				else if (newPlayers[leaderId].maxWinnings < newPot)
				{
					let leaderWinnings = Math.round(newPlayers[leaderId].maxWinnings/2)
					let remainingPot = newPot - leaderWinnings
					newPlayers[leaderId].chips = newPlayers[leaderId].chips + leaderWinnings
					newPlayers[tiedWithId].chips = newPlayers[tiedWithId].chips + remainingPot
					newPot = 0
				}
				else if (newPlayers[tiedWithId].maxWinnings < newPot)
				{
					let tiedWithWinnings = Math.round(newPlayers[tiedWithId].maxWinnings/2)
					let remainingPot = newPot - tiedWithWinnings
					newPlayers[tiedWithId].chips = newPlayers[tiedWithId].chips + tiedWithWinnings
					newPlayers[leaderId].chips = newPlayers[leaderId].chips + remainingPot
					newPot = 0
				}
				else
				{
					newPlayers[tiedWithId].chips = newPlayers[tiedWithId].chips + Math.round(newPot/2)
					newPlayers[leaderId].chips = newPlayers[leaderId].chips + Math.round(newPot/2)
					newPot = 0
				}					
			}
			else
			{
				newPlayers[leaderId].chips = newPlayers[leaderId].chips + newPlayers[leaderId].maxWinnings
				const otherPlayer = newPlayers.find(player => player.id != leaderId)
				const otherPlayerIndex = newPlayers.indexOf(otherPlayer)
				newPlayers[otherPlayerIndex].chips += newPot - newPlayers[leaderId].maxWinnings
				newPot = 0
			}
			const playersWithChips = newPlayers.filter(player => {return player.chips >0}).length > 1 ? true : false
			return {
				enoughPlayersWithChips: playersWithChips,
				turn: newTurn,
				statusUpdate: newStatus,
				players: newPlayers,
				pot: newPot,
				gameOver: true
			}
		})
	}
	
	updateScore(playerNum,hand) {
		let newHandScore = 1
		//use spread operator to create copy of array; sort by score
		let sortedHand = [...hand].sort((a,b) => a.score - b.score)
		//let cardsCanDiscard = this.state.players[playerNum].cardsAllowed
		let highRemaining = 0
		let pairOne = 0
		let pairTwo = 0
		let threeKind = 0
		let fourKind = 0
		let straightFlush = false
		let flush = true
		let fullHouse = false
		let straight = true
		let tieBreakArray = []
		for (var i=0;i<=4;i++)
		{
			//Update cards allowed to 4 if player has Ace on initial deal (turn 0)
			/*if (this.state.turn == 0 && cardsCanDiscard == 3)
			{
				if (sortedHand[i].score == 14)
				{
					cardsCanDiscard = 4
				}
			}*/
			if (i<4)
			{
				// If next value in array is available and straight is true, check that difference between this and next card is 1; if not set straight to false
				if (straight == true && sortedHand[i+1].score - sortedHand[i].score != 1)
				{
					straight = false
				}
				//set flush false as soon as one card is not the same suit as the next
				if (flush == true && sortedHand[i].suit != sortedHand[i+1].suit)
				{
					flush = false
				}
			}
			//check for pair that is not part of 3 of a kind
			if (i < 4 && sortedHand[i].score == sortedHand[i+1].score && sortedHand[i].score != threeKind)
			{
				//if pair, check for 3, only if 3 of a kind and 4 of a kind not already found
				if (i < 3 && sortedHand[i].score == sortedHand[i+2].score && threeKind == 0 && fourKind == 0)
				{
					//if 3, check for 4 and set value for high remaining card
					if (i < 2 && sortedHand[i].score == sortedHand[i+3].score)
					{
						fourKind = sortedHand[i].score
						//set value for remaining high card - if this is 1st iteration, it's at the end of the array; otherwise it's at the beginning
						if (i == 0)
						{
							highRemaining = sortedHand[i+4].score
						}
						else
						{
							highRemaining = sortedHand[i-1].score
						}
					}
					else
					{
						threeKind = sortedHand[i].score
						//if already have a pair, this is a full house
						if (pairOne > 0)
						{
							fullHouse = true
						}
					}
				}
				else
				{
					if (pairOne > 0)
					{
						//if pair already exists, add second pair value; note that since the array is sorted, this will be the higher value pair and gets more weighting below
						pairTwo = sortedHand[i].score
					}
					else
					{
						pairOne = sortedHand[i].score
						//if already have 3 of a kind, this is a full house
						if (threeKind > 0)
						{
							fullHouse = true
						}
					}
				}
			}
			else if (sortedHand[i].score > highRemaining && sortedHand[i].score != threeKind && sortedHand[i].score != pairOne && sortedHand[i].score != pairTwo)
			{
				if (highRemaining > 0)
				{
					tieBreakArray.push(highRemaining)
				}
				// set high card if higher than existing or than initial 0 value, only if not included in 3 of a kind or pair
				highRemaining = sortedHand[i].score
			}
		}
		
		if (flush && straight)
		{
			straightFlush = true
		}
		/*
		Allocate scores in columns to avoid need for comparing cards, except in flush, 3 of a kind, pair or high card where there is a tie after high card
		In the case of tie break, use tie break array, which contains remaining sequence of high cards
		straight flush 		90000
		royal flush 		90014 
		four of a kind 		81409 aces and 9 remaining
		full house 			71302 kings and twos
		flush 				60000
		flush king high		60013
		straight 			50000
		straight queen high	50012
		three of a kind		40000
		three 4s an ace		40414
		two pairs 			30000
		one pair 			20000
		high card 			10000*/
		if (straightFlush)
		{
			newHandScore = 90000 + highRemaining
		}
		else if (fourKind > 0)
		{
			newHandScore = 80000 + (fourKind * 100) + highRemaining
		}
		else if (fullHouse)
		{
			newHandScore = 70000 + (threeKind * 100) + pairOne
		}
		else if (flush)
		{
			newHandScore = 60000 + highRemaining
			// requires tie break using tie break array
		}
		else if (straight)
		{
			newHandScore = 50000 + highRemaining
		}
		else if (threeKind > 0)
		{
			newHandScore = 40000 + (threeKind * 100) + highRemaining
			// requires tie break using tie break array
		}
		else if (pairTwo > 0)
		{
			newHandScore = 30000 + (pairTwo * 100) + pairOne
			// requires tie break using highest card
		}
		else if (pairOne > 0)
		{
			newHandScore = 20000 + (pairOne * 100) + highRemaining
			// requires tie break using tie break array
		}
		else
		{
			newHandScore = 10000 + highRemaining
			// requires tie break using tie break array
		}
		this.setState(prevState => {
			const newPlayers = prevState.players
			//newPlayers[playerNum].cardsAllowed = cardsCanDiscard
			newPlayers[playerNum].pairOneValue = pairOne
			newPlayers[playerNum].pairTwoValue = pairTwo
			newPlayers[playerNum].threeKindValue = threeKind
			newPlayers[playerNum].fourKindValue = fourKind
			newPlayers[playerNum].highestCard = highRemaining
			newPlayers[playerNum].tieBreakValues = tieBreakArray
			newPlayers[playerNum].handScore = newHandScore
			return {
				players: newPlayers
			}
		})
		}
	
	updateHand(playerNum,cardPicked) {
		this.setState(prevState => {
		const newPlayers = prevState.players
		newPlayers[playerNum].hand.push(cardPicked)
		//place initial ante on dealing and update hand scores
		if (this.state.turn == 0 && newPlayers[playerNum].hand.length == 5)
		{
			this.bet(playerNum,1)
			this.updateScore(playerNum,newPlayers[playerNum].hand)
		}
		//update scores after draw and make sure it's the turn of the starting player
		else if (this.state.turn == 3 && newPlayers[playerNum].hand.length == 5)
		{
			this.updateScore(playerNum,newPlayers[playerNum].hand)
			if (newPlayers[playerNum].startingPlayer === true)
			{
				newPlayers[playerNum].myturn = true												
			}
			else
			{
				newPlayers[playerNum].myturn = false												
			}
		}
		return {
			players: newPlayers
		}
		}, () => {
			const checkDeal = this.state.players.filter(player => {return player.hand.length <5})
			this.setState(prevState => {
			let finishedDealing = true
			if (checkDeal.length > 0)
			{
				finishedDealing = false
			}
			return {
				odd: true,
				readyNext: finishedDealing
			}
		})
		}
	)
	}

	
	deal(playerNum) {
		const aCard = this.selectCard()
		this.updateDeck(aCard)
		this.updateHand(playerNum,aCard)
	}
	
	discard(playerNum,aCard) {
		this.setState(prevState => {
		const newPlayers = prevState.players
		const cardToDiscard = newPlayers[playerNum].hand.find(cardNum => cardNum.id == aCard)
		const discardIndex = newPlayers[playerNum].hand.indexOf(cardToDiscard)
		newPlayers[playerNum].hand.splice(discardIndex,1)
		newPlayers[playerNum].buffer.push(cardToDiscard)
		return {
			players: newPlayers
		}
		}
	)
	}

	discardDone(playerNum) {
		this.setState(prevState => {
		const newPlayers = prevState.players
		const cardString = newPlayers[playerNum].buffer.length == 1 ? "card" : "cards"
		const newStatus = newPlayers[playerNum].name + " discarded " + newPlayers[playerNum].buffer.length + " " + cardString
		newPlayers[playerNum].discarded = true
		if (newPlayers[playerNum].startingPlayer === true)
		{
			newPlayers[playerNum].myturn = true												
		}
		else
		{
			newPlayers[playerNum].myturn = false												
		}
		return {
			players: newPlayers,
			statusUpdate : newStatus
		}
		}, () => {
			const checkDiscarded = this.state.players.filter(player => {return player.discarded === false})
			if (checkDiscarded.length == 0)
			{
				this.setState( {
				readyNext: true
				})
			}	
		}
	)
	}
	
	compDiscard(playerNum)
	{
		// Discard no cards if straight flush, flush, straight or full house
		if (this.state.players[playerNum].handScore >= 90000 || (this.state.players[playerNum].handScore >= 50000 && this.state.players[playerNum].handScore < 80000) )
		{
			this.discardDone(playerNum)
		}
		else if (this.state.players[playerNum].fourKindValue > 0)
		{
			const potentialDiscard = this.state.players[playerNum].hand.find(cardNum => cardNum.score != this.state.players[playerNum].fourKindValue)
			if (potentialDiscard.score < (6 + this.state.players[playerNum].character))
			{
				this.setState(prevState => {
				let newPlayers = prevState.players
				const discardIndex = newPlayers[playerNum].hand.indexOf(potentialDiscard)
				newPlayers[playerNum].hand.splice(discardIndex,1)
				newPlayers[playerNum].buffer.push(potentialDiscard)
				return {
					players: newPlayers
				}
				}, () => {this.discardDone(playerNum)}
				)
			}
		}
		// if two pairs, always discard remaining card, as you might get fh
		else if (this.state.players[playerNum].handScore >= 30000 && this.state.players[playerNum].handScore < 40000)
		{	
			this.setState(prevState => {
			let newPlayers = prevState.players
			const cardToDiscard = this.state.players[playerNum].hand.find(cardNum => cardNum.score != this.state.players[playerNum].pairOneValue && cardNum.score != this.state.players[playerNum].pairTwoValue)
			const discardIndex = newPlayers[playerNum].hand.indexOf(cardToDiscard)
			newPlayers[playerNum].hand.splice(discardIndex,1)
			newPlayers[playerNum].buffer.push(cardToDiscard)
			return {
				players: newPlayers
			}
			}, () => {this.discardDone(playerNum)}
			)
			
		}
		else if (this.state.players[playerNum].threeKindValue > 0)
		{
			const discardArray = this.state.players[playerNum].hand.filter(card => card.score != this.state.players[playerNum].threeKindValue)	
			this.setState(prevState => {
			let newPlayers = prevState.players
			for (var i=0; i < discardArray.length; i++)
			{
				const cardToDiscard = newPlayers[playerNum].hand.find(cardNum => cardNum.id == discardArray[i].id)
				const discardIndex = newPlayers[playerNum].hand.indexOf(cardToDiscard)
				newPlayers[playerNum].hand.splice(discardIndex,1)
				newPlayers[playerNum].buffer.push(cardToDiscard)
			}
			return {
				players: newPlayers
				}
			}, () => {this.discardDone(playerNum)}
			)

		}
		else
		{
			//check for 4 cards of same suit - if found and have pair discard one card of different suit if risky player (character 3); otherwise discard three lowest cards
			const heartsSuit = this.state.players[playerNum].hand.filter(card => card.suit == "hearts").length
			const clubsSuit = this.state.players[playerNum].hand.filter(card => card.suit == "clubs").length
			const diamondsSuit = this.state.players[playerNum].hand.filter(card => card.suit == "diamonds").length
			const spadesSuit = this.state.players[playerNum].hand.filter(card => card.suit == "spades").length
			let longSuit = false
			if (heartsSuit == 4)
			{
				longSuit = "hearts"
			}
			else if (clubsSuit == 4)
			{
				longSuit = "clubs"
			}
			else if (diamondsSuit == 4)
			{
				longSuit = "diamonds"
			}	
			else if (spadesSuit == 4)
			{
				longSuit = "spades"
			}
			// If has 4 cards of same suit and either doesn't have a pair or is a risky player and prepared to discard one of a pair for a chance of all 5 cards of same suit
			if (longSuit && ((this.state.players[playerNum].pairOneValue == 0) || (this.state.players[playerNum].character == 3)))
			{
				this.setState(prevState => {
					let newPlayers = prevState.players
					const cardToDiscard = newPlayers[playerNum].hand.find(card => card.suit != longSuit)
					const discardIndex = newPlayers[playerNum].hand.indexOf(cardToDiscard)
					newPlayers[playerNum].hand.splice(discardIndex,1)
					newPlayers[playerNum].buffer.push(cardToDiscard)
					return {
						players: newPlayers
					}
					},  () => {this.discardDone(playerNum)}
					)
			}
			// If has pair, discard all other cards
			else if (this.state.players[playerNum].pairOneValue > 0)
			{
				const discardArray = this.state.players[playerNum].hand.filter(card => { return card.score != this.state.players[playerNum].pairOneValue})
				this.setState(prevState => {
				let newPlayers = prevState.players
				for (var i=0; i < discardArray.length; i++)
				{
					const cardToDiscard = newPlayers[playerNum].hand.find(cardNum => cardNum.id == discardArray[i].id)
					const discardIndex = newPlayers[playerNum].hand.indexOf(cardToDiscard)
					newPlayers[playerNum].hand.splice(discardIndex,1)
					newPlayers[playerNum].buffer.push(cardToDiscard)
				}
				return {
					players: newPlayers
				}
				}, () => {this.discardDone(playerNum)}
				)				
			}
			// Else discard three lowest cards
			else
			{
				let sortedHand = [...this.state.players[playerNum].hand].sort((a,b) => a.score - b.score)
				this.setState(prevState => {
				let newPlayers = prevState.players
				for (var i=0; i<=2; i++)
				{
					const cardToDiscard = newPlayers[playerNum].hand.find(cardNum => cardNum.id == sortedHand[i].id)
					const discardIndex = newPlayers[playerNum].hand.indexOf(cardToDiscard)
					newPlayers[playerNum].hand.splice(discardIndex,1)
					newPlayers[playerNum].buffer.push(cardToDiscard)
				}
				return {
					players: newPlayers
				}
				}, () => {this.discardDone(playerNum)}
				)
			}
		}	
	}
	
	compMove(playerNum,ableToCheck) {
		
		/*					% AVAILABLE CHIPS WILLING TO BET
							1			2			3
		SCORE 	> 	81000 	10-90		20-100		30-100	
					80000	10-80		10-90		30-100
					70000	10-80		10-80		30-100
					60000	10-70		10-80		25-100
					50000	5-60		5-70		20-100
					40000	5-50		5-60		10-100
					30000	5-40		5-60		10-100
					20000	0-30		0-50		5-100
					10000	0-25		0-40		0-100
		*/
		const scoreBands = [
		10000,
		20000,
		30000,
		40000,
		50000,
		60000,
		70000,
		80000,
		81000
		]
		//Get place in arrays to determine % willing to bet and modifiers
		const placeInBand = this.state.players[playerNum].handScore > 81000 ? 8 : Math.floor(this.state.players[playerNum].handScore/10000) - 1		
		const characterPlace = this.state.players[playerNum].character - 1
		// Array of % willing to bet; each pair shows min/max values; 3 arrays for 3 character types
		const willingArray = 
		[
		[
		[0,25],
		[0,30],
		[5,40],
		[5,50],
		[5,60],
		[10,70],
		[10,80],
		[10,80],
		[10,90]		
		],
		[
		[0,40],
		[0,50],
		[5,60],
		[5,60],
		[5,70],
		[10,80],
		[10,80],
		[10,90],
		[20,100]		
		],
		[
		[0,100],
		[5,100],
		[10,100],
		[10,100],
		[20,100],
		[25,100],
		[30,100],
		[30,100],
		[30,100]		
		]
		]
		
		const minBet = willingArray[characterPlace][placeInBand][0]
		const maxBet = willingArray[characterPlace][placeInBand][1]
		//Get % of chips willing to bet by getting random integer between min and max values
		let willingToBetPC = Math.floor(Math.random() * (maxBet - minBet)) + minBet
		
		
		/*MODIFIERS	TO % WILLING TO BET	IF SCORE < 70K			
									1	2	3
		
		<= 0.5 OP'S CHIPS			-10	-5	0
		OR
		OPPONENT DREW 1 CARD		-10	-5	0
		*/
		
		const halfChips = this.state.players.filter(aPlayer => {return aPlayer.chips > (this.state.players[playerNum].chips * 2)}).length > 0 ? true : false
		//if turn == 4 draw has taken place; and if player has drawn at least two cards; and if another player's buffer array < 2 in length, opponent drew fewer than two cards
		const poorDraw = this.state.turn == 4 && this.state.players.filter(aPlayer => {return aPlayer.id != this.state.players[playerNum].id && aPlayer.buffer.length < 2 }).length > 0 ? true : false
		//Apply modifiers to reduce % willing to bet if an opponent has double player's chips or if opponent drew less than two cards
		if ((halfChips || poorDraw) && (this.state.players[playerNum].handScore < 70000))
		{
			willingToBetPC -= (10/this.state.players[playerNum].character)
			if (willingToBetPC < 0)
			{
				willingToBetPC = 0
			}
			else if (willingToBetPC > 100)
			{
				willingToBetPC = 100
			}
		}
		// Get max chips able to bet - if player hasn't yet raised, this is number of chips; if they have, it's highest bet minus their current bet (i.e. amount required to see)
		let	maxAllowed = this.state.players[playerNum].chips
		const otherPlayer = this.state.players.filter(aPlayer => {return aPlayer.id != this.state.players[playerNum].id})
		// If other player has no chips, limit bet to matching highest bet - no point raising stakes when opponent can't match
		// Also limit bet if already raised in round
		if ((this.state.players[playerNum].raised == true) || (otherPlayer[0].chips == 0))
		{
			maxAllowed = this.state.highestBet - this.state.players[playerNum].currentBet
		}
		// *************************************************
		if (this.state.odd === true && this.state.players[playerNum].raised == false)
		{
			maxAllowed = otherPlayer[0].chips
		}
		//Get actual number of chips willing to bet
		let chipsToBet = Math.floor(this.state.players[playerNum].chips * (willingToBetPC/100))
		//If want to bet more than allowed, reduce stake to allowed
		if (chipsToBet > maxAllowed)
		{
			chipsToBet = maxAllowed
		}
		if ((chipsToBet == 0 && ableToCheck == true) || (this.state.players[playerNum].raised === true && maxAllowed == 0  && ableToCheck == true))
		{
			this.check(playerNum)
		}
		//CASE 1 If amount willing to bet matches or exceeds what's required to see highest bet, place bet
		else if (chipsToBet >= this.state.highestBet - this.state.players[playerNum].currentBet)
		{
			if (chipsToBet == 0)
			{
				this.check(playerNum)				
			}
			else
			{
				this.bet(playerNum,chipsToBet)		
			}

		}
		//else if willing to bet less than required to see opponent
		else if (chipsToBet < this.state.highestBet - this.state.players[playerNum].currentBet)
		{
			// 3 arrays for scores and character types (1 == conservative to 3 == risk taker); more conservative means more willing to fold
			const foldChancesArray = [[70,60,50,40,30,20,10,5,0],[60,50,40,30,20,10,2,1,0],[30,20,20,15,10,5,0,0,0]]
			let foldChances = foldChancesArray[characterPlace][placeInBand]
			//If this is first turn - players haven't drawn - reduce chances of folding
			if (this.state.turn == 1)
			{
				foldChances -= 20
				if (foldChances < 0)
				{
					foldChances = 0
				}
			}
			//If random number between 0 and 100 less than chances of folding, fold
			if (Math.floor(Math.random() * 101) < foldChances)
			{
				this.fold(playerNum)
			}
			//CASE 2 If have enough chips to cover min required bet, bet that amount
			else if (this.state.players[playerNum].chips >= this.state.highestBet - this.state.players[playerNum].currentBet)
			{
				if (this.state.highestBet - this.state.players[playerNum].currentBet == 0)
				{
					this.check(playerNum)
				}
				else
				{
					this.bet(playerNum,this.state.highestBet - this.state.players[playerNum].currentBet)
				}
			}
			// CASE 3 If don't have enough chips, go all in
			else
			{
				this.bet(playerNum,this.state.players[playerNum].chips)
			}
		}
	}
	
	render() {
		return (
		<div>
		<div id="playerSelection" style={{display: this.state.started && "none"}}>
		<p>I built this app as part of the process of learning React, as well as exploring best practices in terms of service workers, caching and progressive web apps.</p>
		<p>It's a simple version of draw poker.</p>
		<p>Each player starts with ten chips.</p>
		<p>Once the cards have been dealt, a player can:</p>
			<ul>
			<li><strong>check</strong> &ndash; i.e. not add anything to the pot</li>
			<li><strong>bet</strong> &ndash; as long as they have enough chips, or</li>
			<li><strong>fold</strong> &ndash; exit the game and forfeit their stake.</li>
			</ul>
		<p>Each player automatically adds one (the ante) to the pot at the start of the game, and each player can raise the stakes once in a given round.</p>
		<p>If a player can't match their opponent's stake, they can go <strong>all in</strong> (bet all their remaining chips) to remain in the game, although this limits their potential winnings based on what they have added to the pot.</p>
		<p>After the first round of betting, each player may discard up to three cards and substitute them for new cards from the pack, before the final round of betting.</p>
		<p>The game is over when a player folds or when all the betting is done and the players' hands are compared.</p>
		<p>At this point, you can play another round. On starting a new round, the players switch places &ndash; if you opened the betting in the first round, your opponent will open in the second.</p>
		<p>The match is over when a game finishes and one of the players has run out of chips.</p>
		<h2>Select players</h2>
		<p><label htmlFor="p_a">Player 1 </label><input type="text" placeholder={this.state.computerPlayer === 0 ? "Name for computer player" : "Your name"} id="p_a" name="p_a" /></p>
		<p><label htmlFor="p_b">Player 2 </label><input type="text" placeholder={this.state.computerPlayer === 1 ? "Name for computer player" : "Your name"} id="p_b" name="p_b" /></p>
		<p><label htmlFor="r1"><input type="radio" id="r1" name="computerPlayer" value="0" checked={this.state.computerPlayer === 0} onChange={this.handleChange} />Player 1 is computer</label><br />
		<label htmlFor="r2"><input type="radio" id="r2" name="computerPlayer" value="1" checked={this.state.computerPlayer === 1} onChange={this.handleChange} />Player 2 is computer</label></p>
		<button id="playersSelect" onClick={this.handleClickSetup} style={{marginRight: "8px"}}>Confirm</button>
		<button style={{display: this.state.devMode ? "block" : "none"}} onClick={this.checkState}>Check state</button>
		</div>
		<div id="tabl" style={{display: this.state.started ? "block" : "none"}}>
		{this.state.statusUpdate && <h2 className="status">{this.state.statusUpdate}</h2>}
		{(this.state.gameOver && !this.state.enoughPlayersWithChips) && <h2 className="status">The match is over! Hit the reset button to start a new one.</h2>}
		{this.state.players.map(key => <Player key={key.id} playerNum={key.id} name={key.name} hand={key.hand} chips={key.chips} currentBet={key.currentBet} maxWinnings={key.maxWinnings} discarded={key.discarded} drawn={key.drawn} myturn={key.myturn} won={key.won} raised={key.raised}  handScore={key.handScore} buffer={key.buffer} character={key.character} compMove={this.compMove} compDiscard={this.compDiscard} discard={this.discard} discardDone={this.discardDone} bet={this.bet} deal={this.deal} check={this.check} fold={this.fold} readyNext={this.state.readyNext} devMode={this.state.devMode} turn={this.state.turn} highestBet={this.state.highestBet} gameOver={this.state.gameOver} />)}
		<button style={{display: this.state.readyNext ? "block" : "none"}} onClick={this.next}>Next&gt;&gt;</button><br />
		<button style={{display: this.state.turn == 5 && this.state.gameOver == false ? "block" : "none"}} onClick={this.checkHands}>Compare hands</button>
		<button style={{display: this.state.devMode ? "block" : "none"}} onClick={this.checkState}>Check state</button><br />
		
		<div>
		<button style={{display: this.state.started && this.state.gameOver && this.state.enoughPlayersWithChips ? "block" : "none"}} onClick={this.newRound}>New round</button><br />
		<button style={{display: this.state.started ? "block" : "none"}} onClick={this.abandon}>Reset game</button>
		</div>
		
		</div>
		</div>
		)
	}
}

export default Setup